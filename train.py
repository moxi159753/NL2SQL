import torch
from sqlnet.utils import *
from sqlnet.model.sqlnet import SQLNet

import argparse

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    # 批处理大小，外部指定了64
    parser.add_argument('--bs', type=int, default=16, help='Batch size')

    # 迭代的次数
    parser.add_argument('--epoch', type=int, default=100, help='Epoch number')

    # 是否开启ＧＰＵ加速
    parser.add_argument('--gpu', action='store_true', help='Whether use gpu to train')

    # 如果设置了，使用小数据进行快速调试
    parser.add_argument('--toy', action='store_true', help='If set, use small data for fast debugging')

    # 是否使用列注意
    parser.add_argument('--ca', action='store_true', help='Whether use column attention')

    # 为SQLNet训练单词向量
    parser.add_argument('--train_emb', action='store_true', help='Train word embedding for SQLNet')

    # 是否还原训练过的模型
    parser.add_argument('--restore', action='store_true', help='Whether restore trained model')

    # 保存实验日志的路径
    parser.add_argument('--logdir', type=str, default='', help='Path of save experiment log')

    args = parser.parse_args()

    n_word=300

    if args.toy:
        use_small=True
        gpu=args.gpu
        batch_size=16
    else:
        use_small=False
        gpu=args.gpu
        batch_size=args.bs

    # 学习速率
    learning_rate = 1e-3

    # 加载数据集
    train_sql, train_table, train_db, dev_sql, dev_table, dev_db = load_dataset(use_small=use_small)

    # 加载词向量
    word_emb = load_word_emb('data/char_embedding.json')

    # 初始化得到模型
    model = SQLNet(word_emb, N_word=n_word, use_ca=args.ca, gpu=gpu, trainable_emb=args.train_emb)

    # 使用优化器　Adam：  传入模型的全部参数， 学习率
    # weight decay（权值衰减）使用的目的是防止过拟合。在损失函数中，weight decay是放在正则项（regularization）前面的一个系数，正则项一般指示模型的复杂度，所以weight decay的作用是调节模型复杂度对损失函数的影响，若weight decay很大，则复杂的模型损失函数的值也就大。
    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate, weight_decay=0)

    # 如果设置了 --restore，那么从保存的模型取出来，继续训练
    if args.restore:
        model_path= 'saved_model/best_model'
        print ("Loading trained model from %s" % model_path)
        model.load_state_dict(torch.load(model_path))

    # 用于记录每个子任务的最佳得分
    best_sn, best_sc, best_sa, best_wn, best_wc, best_wo, best_wv, best_wr = 0, 0, 0, 0, 0, 0, 0, 0
    best_sn_idx, best_sc_idx, best_sa_idx, best_wn_idx, best_wc_idx, best_wo_idx, best_wv_idx, best_wr_idx = 0, 0, 0, 0, 0, 0, 0, 0
    best_lf, best_lf_idx = 0.0, 0
    best_ex, best_ex_idx = 0.0, 0

    print ("#"*20+"  Star to Train  " + "#"*20)
    for i in range(args.epoch):
        print ('Epoch %d'%(i+1))

        # 在train dataset 上进行训练
        train_loss = epoch_train(model, optimizer, batch_size, train_sql, train_table)

        # 在 dev dataset 上进行评估
        dev_acc = epoch_acc(model, batch_size, dev_sql, dev_table, dev_db)

        # 每个子任务的准确性
        print ('Sel-Num: %.3f, Sel-Col: %.3f, Sel-Agg: %.3f, W-Num: %.3f, W-Col: %.3f, W-Op: %.3f, W-Val: %.3f, W-Rel: %.3f'%(
            dev_acc[0][0], dev_acc[0][1], dev_acc[0][2], dev_acc[0][3], dev_acc[0][4], dev_acc[0][5], dev_acc[0][6], dev_acc[0][7]))

        # 保存最好的模型
        if dev_acc[1] > best_lf:
            best_lf = dev_acc[1]
            best_lf_idx = i + 1
            torch.save(model.state_dict(), 'saved_model/best_model')

        if dev_acc[2] > best_ex:
            best_ex = dev_acc[2]
            best_ex_idx = i + 1

        # 记录每个子任务的最好成绩
        if True:
            if dev_acc[0][0] > best_sn:
                best_sn = dev_acc[0][0]
                best_sn_idx = i+1
            if dev_acc[0][1] > best_sc:
                best_sc = dev_acc[0][1]
                best_sc_idx = i+1
            if dev_acc[0][2] > best_sa:
                best_sa = dev_acc[0][2]
                best_sa_idx = i+1
            if dev_acc[0][3] > best_wn:
                best_wn = dev_acc[0][3]
                best_wn_idx = i+1
            if dev_acc[0][4] > best_wc:
                best_wc = dev_acc[0][4]
                best_wc_idx = i+1
            if dev_acc[0][5] > best_wo:
                best_wo = dev_acc[0][5]
                best_wo_idx = i+1
            if dev_acc[0][6] > best_wv:
                best_wv = dev_acc[0][6]
                best_wv_idx = i+1
            if dev_acc[0][7] > best_wr:
                best_wr = dev_acc[0][7]
                best_wr_idx = i+1
        print ('Train loss = %.3f' % train_loss)

        # 输出精度和准确性
        print ('Dev Logic Form Accuracy: %.3f, Execution Accuracy: %.3f' % (dev_acc[1], dev_acc[2]))

        print ('Best Logic Form: %.3f at epoch %d' % (best_lf, best_lf_idx))

        print ('Best Execution: %.3f at epoch %d' % (best_ex, best_ex_idx))

        if (i+1) % 10 == 0:
            print ('Best val acc: %s\nOn epoch individually %s'%(
                    (best_sn, best_sc, best_sa, best_wn, best_wc, best_wo, best_wv),
                    (best_sn_idx, best_sc_idx, best_sa_idx, best_wn_idx, best_wc_idx, best_wo_idx, best_wv_idx)))
