
## 数据说明
本次赛题将提供4万条有标签数据作为训练集，1万条无标签数据作为测试集。其中，5千条测试集数据作为初赛测试集，对选手可见；5千条作为复赛测试集，对选手不可见。

提供的数据集主要由3个文件组成，以训练集为例，包括train.json、train.tables.json及train.db。

train.json文件中，每一行为一条数据样本。数据样例及字段说明例如下：

```
{
     "table_id": "a1b2c3d4", # 相应表格的id
     "question": "世茂茂悦府新盘容积率大于1，请问它的套均面积是多少？", # 自然语言问句
    "sql":{ # 真实SQL
        "sel": [7], # SQL选择的列 
        "agg": [0], # 选择的列相应的聚合函数, '0'代表无
        "cond_conn_op": 0, # 条件之间的关系
        "conds": [
            [1,2,"世茂茂悦府"], # 条件列, 条件类型, 条件值，col_1 == "世茂茂悦府"
            [6,0,1]
        ]
    }
}

```

其中，SQL的表达字典说明如下：

```
op_sql_dict = {0:">", 1:"<", 2:"==", 3:"!="}
agg_sql_dict = {0:"", 1:"AVG", 2:"MAX", 3:"MIN", 4:"COUNT", 5:"SUM"}
conn_sql_dict = {0:"", 1:"and", 2:"or"}

```

train.tables.json 文件中，每一行为一张表格数据。数据样例及字段说明例如下：

```
{
    "id":"a1b2c3d4", # 表格id
    "name":"Table_a1b2c3d4", # 表格名称
    "title":"表1：2019年新开工预测 ", # 表格标题
    "header":[ # 表格所包含的列名
        "300城市土地出让",
        "规划建筑面积(万㎡)",
        ……
    ],
    "types":[ # 表格列所相应的类型
        "text",
        "real",
        ……
    ],
    "rows":[ # 表格每一行所存储的值
        [
            "2009年7月-2010年6月",
            168212.4,
            ……
        ]
    ]
}
```

tables.db为sqlite格式的数据库形式的表格文件。各个表的表名为tables.json中相应表格的name字段。为避免部分列名中的特殊符号导致无法存入数据库文件，表格中的列名为经过归一化的字段，col_1, col_2, …, col_n。db文件将后续更新。

另外，也提供用于baseline方案的字向量文件char_embedding，每一行的内容为字符及其300维的向量表达，以空格分隔。

## 介绍

这个基线方法是基于SQLNet的代码开发和改进的，SQLNet是WikiSQL中的一个基线模型。
该模型将生成整个SQL的任务解耦为多个子任务，包括选择号、选择列、选择聚合、条件号、条件列等。
这里显示的是简单的模型结构，实现细节可以参考原文。
## 依赖

 - Python 2.7
 - torch 1.0.1
 - records 0.5.3
 - tqdm

## 开始训练

首先，下载提供的数据集~/data_nl2sql/，其中应该包括train。json, train.tables。json、val.json val.tables。json和char_embedded，并将它们划分为以下结构。
```
├── data_nl2sql
│ ├── train
│ │ ├── train.db
│ │ ├── train.json
│ │ ├── train.tables.json
│ ├── val
│ │ ├── val.db
│ │ ├── val.json
│ │ ├── val.tables.json
│ ├── test
│ │ ├── test.db
│ │ ├── test.json
│ │ ├── test.tables.json
│ ├── char_embedding.json
```

然后

```
mkdir ~/nl2sql
cd ~/nl2sql/
git clone https://github.com/ZhuiyiTechnology/nl2sql_baseline.git

cp -r ~/data_nl2sql/* ~/nl2sql/nl2sql_baseline/data/
cd ~/nl2sql/nl2sql_baseline/

sh ./start_train.sh 0 64
```
第一个参数0表示gpu数量，第二个参数表示批大小。

## 开始评估

在var.json或test上求值，确保已训练好的模型就绪，然后运行
```
cd ~/nl2sql/nl2sql_baseline/
sh ./start_test.sh 0 pred_example
```
第一个参数0表示gpu数量，第二个参数表示预测的输出路径。

## 实验结果

我们进行了多次实验，在128个批次的val数据集上实现了27.5%的逻辑形式精度。


## 实验分析

我们发现该数据集的主要挑战包括条件值预测差、NL问题中未提及的select列和condition列、NL问题与SQL之间条件关系表示不一致等。所有这些挑战都不能通过现有的基线和SOTA模型来解决。
与之相对应的是，即使在训练集上，这个基线模型对条件列的准确率也只有77%，对条件值的准确率也只有62%，整体逻辑形式也只有50%左右，说明这些问题对于参赛者来说是具有挑战性的。

<div align="middle"><img src="https://github.com/ZhuiyiTechnology/nl2sql_baseline/blob/master/img/trainset_behavior.png"width="80%" ></div>

## 相关资源
https://github.com/salesforce/WikiSQL

https://yale-lily.github.io/spider

<a href="https://www.jiqizhixin.com/articles/2018-10-24-11">你已经是个成熟的表格了，该学会自然语言处理了</a>

<a href="https://www.jianshu.com/p/53e379483f3e">经典检索算法：BM25原理</a>


<a href="https://arxiv.org/pdf/1804.08338.pdf">Semantic Parsing with Syntax- and Table-Aware SQL Generation</a>
